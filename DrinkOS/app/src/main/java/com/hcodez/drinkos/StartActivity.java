package com.hcodez.drinkos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.Tag;
import com.hcodez.drinkos.Models.User;

import org.w3c.dom.Text;

public class StartActivity extends AppCompatActivity {

    Button btn_start;
    EditText txt_nickname;
    EditText txt_password;
    LinearLayout layout_start;
    private DatabaseReference mDatabase;
    boolean password__Incorrect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        InitializeUI();
    }

    private void InitializeUI() {
        btn_start = findViewById(R.id.btn_start);
        txt_nickname = findViewById(R.id.txt_nickname);
        txt_password = findViewById(R.id.txt_password);
        layout_start = findViewById(R.id.layout_start);
        mDatabase =  FirebaseDatabase.getInstance().getReference();

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(txt_nickname.getText().toString()) || TextUtils.isEmpty(txt_password.getText().toString())) {
                    infoSnackBar("Nickname and password are required!", "Gotcha!");
                }
                else if(txt_nickname.getText().toString().length() < 3) {
                    infoSnackBar("Nickname must be 3 characters or longer!", "Gotcha!");
                }
                else if(txt_password.getText().toString().length() < 6) {
                    infoSnackBar("Password must be 6 characters or longer!", "Gotcha!");
                }
                else {
                   loginUser(new User(txt_nickname.getText().toString(), txt_password.getText().toString(), txt_nickname.getText().toString().toLowerCase(), "Unknown"));
                }
            }
        });
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void infoSnackBar (String info, String btn_info) {
        hideKeyboard(StartActivity.this);
        Snackbar.make(layout_start, info, Snackbar.LENGTH_LONG)
                .setAction(btn_info, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) { }
                })
                .setActionTextColor(getResources().getColor(R.color.colorAccent))
                .show();
    }

    private void loginUser(final User newUser) {
        DatabaseReference ref = mDatabase.child("Users");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                password__Incorrect = false;
                for(DataSnapshot data : dataSnapshot.getChildren()) {
                    User mUser = data.getValue(User.class);
                    if(mUser.getSearch_name().equals(newUser.getSearch_name()) && !mUser.getPassword().equals(newUser.getPassword())) {
                        infoSnackBar("Password is incorrect!", "Gotcha!");
                        password__Incorrect = true;
                    }
                    else if(mUser.getSearch_name().equals(newUser.getSearch_name()) && mUser.getPassword().equals(newUser.getPassword())) {
                        Intent intent = new Intent(StartActivity.this, MainActivity.class);
                        //intent.putExtra(); poslije ces dodat userID za roomove ovdje
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    }
                }
                if(!password__Incorrect) {
                    registerUser(newUser);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

    private void registerUser(final User newUser) {
        Snackbar.make(layout_start, "Ooooh, new user, would You like to register?", Snackbar.LENGTH_LONG)
                .setAction("Yes, please!", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatabaseReference ref = mDatabase.child("Users");
                        ref.child(newUser.getSearch_name()).setValue(newUser);
                    }
                })
                .setActionTextColor(getResources().getColor(R.color.colorAccent))
                .show();
    }
}