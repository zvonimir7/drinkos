package com.hcodez.drinkos.Models;

public class Gang {
    String leader;
    String zone;
    String gang_name;

    public Gang() {
    }

    public Gang(String leader, String zone, String gang_name) {
        this.leader = leader;
        this.zone = zone;
        this.gang_name = gang_name;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getGang_name() {
        return gang_name;
    }

    public void setGang_name(String gang_name) {
        this.gang_name = gang_name;
    }
}
