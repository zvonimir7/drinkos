package com.hcodez.drinkos.Models;

public class User {
    String nickname;
    String password;
    String search_name;
    String gang_name;

    public User(String nickname, String password, String search_name, String gang_name) {
        this.nickname = nickname;
        this.password = password;
        this.search_name = search_name;
        this.gang_name = gang_name;
    }

    public User() {
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSearch_name() {
        return search_name;
    }

    public void setSearch_name(String search_name) {
        this.search_name = search_name;
    }

    public String getGang_name() {
        return gang_name;
    }

    public void setGang_name(String gang_name) {
        this.gang_name = gang_name;
    }
}
