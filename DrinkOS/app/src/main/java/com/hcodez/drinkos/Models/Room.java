package com.hcodez.drinkos.Models;

public class Room {
    private String name;

    public Room(String name) {
        this.name = name;
    }

    public Room() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
