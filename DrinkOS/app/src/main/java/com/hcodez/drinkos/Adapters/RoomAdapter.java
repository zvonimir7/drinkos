package com.hcodez.drinkos.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hcodez.drinkos.MainActivity;
import com.hcodez.drinkos.Models.Gang;
import com.hcodez.drinkos.Models.Room;
import com.hcodez.drinkos.R;

import java.util.List;

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.ViewHolder> {

    private Context mContext;
    private List<Room> mRooms;

    public RoomAdapter(Context mContext, List<Room> mRooms) {
        this.mRooms = mRooms;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.room_cell, parent, false);
        return new RoomAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Room room = mRooms.get(position);
        holder.name.setText(room.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(mContext);
                dialog.setContentView(R.layout.room_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                getGangInfoFromDB(dialog, mRooms.get(position).getName());

                dialog.show();
                Button dialogRoom_join = (Button) dialog.findViewById(R.id.dialogRoom_join);
                dialogRoom_join.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //implementacija za joinanje u gang
                    }
                });
                Button dialogRoom_close = (Button) dialog.findViewById(R.id.dialogRoom_close);
                dialogRoom_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    private void initializeDialogUI(Dialog dialog, Gang gangInfo) {
        TextView dialogRoom_leader = (TextView) dialog.findViewById(R.id.dialogRoom_leader);
        TextView dialogRoom_desc = (TextView) dialog.findViewById(R.id.dialogRoom_desc);
        TextView dialogRoom_zone = (TextView) dialog.findViewById(R.id.dialogRoom_zone);
        ImageView dialogRoom_skin = (ImageView) dialog.findViewById(R.id.dialogRoom_skin);
        Button dialogRoom_join = (Button) dialog.findViewById(R.id.dialogRoom_join);
        dialogRoom_join.setEnabled(true);

        dialogRoom_leader.setText("Current gang leader: " + gangInfo.getLeader());
        dialogRoom_zone.setText("Control zone: " + gangInfo.getZone());

        if (gangInfo.getGang_name().equals("Varrios Los Aztecas")) {
            dialogRoom_desc.setText(R.string.aztecas_desc);
            dialogRoom_skin.setImageResource(R.drawable.aztecas_skin);
        } else if (gangInfo.getGang_name().equals("Ballas")) {
            dialogRoom_desc.setText(R.string.ballas_desc);
            dialogRoom_skin.setImageResource(R.drawable.ballas_skin);
        } else if (gangInfo.getGang_name().equals("Grove Street Families")) {
            dialogRoom_desc.setText(R.string.groves_desc);
            dialogRoom_skin.setImageResource(R.drawable.groves_skin);
        }
    }

    private void getGangInfoFromDB(final Dialog dialog, final String gangName) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Gangs");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot data : snapshot.getChildren()) {
                    if (data.getKey().equals(gangName)) {
                        Gang gangInfo = data.getValue(Gang.class);
                        initializeDialogUI(dialog, gangInfo);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mRooms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.room_name);
        }
    }
}
