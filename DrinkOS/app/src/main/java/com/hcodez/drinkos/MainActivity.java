package com.hcodez.drinkos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import com.hcodez.drinkos.Adapters.RoomAdapter;
import com.hcodez.drinkos.Models.Room;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recView;
    private RoomAdapter roomAdapter;
    private List<Room> Rooms;
    private RoomAdapter c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUI();
    }

    private void initializeUI() {
        initializeRecyclerUI();
    }

    private void initializeRecyclerUI() {
        recView = MainActivity.this.findViewById(R.id.recView_rooms);
        recView.setHasFixedSize(true);
        recView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        Rooms = new ArrayList<>();

        //add to database
        Rooms.add(new Room("Varrios Los Aztecas"));
        Rooms.add(new Room("Ballas"));
        Rooms.add(new Room("Grove Street Families"));
        Rooms.add(new Room("The Mafia"));
        Rooms.add(new Room("San Fierro Triads"));
        Rooms.add(new Room("San Fierro Rifa"));
        Rooms.add(new Room("Los Santos Vagos"));
        Rooms.add(new Room("Da Nang Boys"));

        roomAdapter = new RoomAdapter(this, Rooms);
        recView.setAdapter(roomAdapter);
    }
}